package com.example.micha.lokalizator.tabs;


import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.micha.lokalizator.database.DatabaseHandler;
import com.example.micha.lokalizator.parser.DirectionJsonParser;
import com.example.micha.lokalizator.R;
import com.example.micha.lokalizator.models.Locations;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;

/**
 * Klasa fragmentu odpowiadająca za mapę
 */
public class SecondTab extends Fragment implements GoogleMap.OnInfoWindowClickListener {

    MapView mMapView;
    DatabaseHandler db = null;
    private List<LatLng> LatLongList = new ArrayList<LatLng>();
    private List<String> titles = new ArrayList<String>();
    private MarkerOptions markerOp;
    List<Locations> listAllEvents = new ArrayList<Locations>();
    SharedPreferences pref, pref_route;

    public SecondTab() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_second_tab, container,
                false);
        try {

            db = new DatabaseHandler(getActivity()); //łaczymy sie z bazą

        } catch (Exception e) {
            Log.e("Err", "Błąd bazy");
        }
        pref = getContext().getSharedPreferences("TRACK", MODE_PRIVATE);
        pref_route = getContext().getSharedPreferences("ROUTE", MODE_PRIVATE);

        listAllEvents.clear();
        listAllEvents = db.getAllEvents(); // Pobranie wszystkich dostępnych lokacji z bazy danych
        db.close();

        mMapView = (MapView) v.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(final GoogleMap googleMap) {

                googleMap.clear();
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                    /**
                     * Metoda odpowiadająca za akcje po kliknięciu na marker
                     */
                    @Override
                    public boolean onMarkerClick(final Marker marker) {


                        Double lati = Double.parseDouble(pref.getString("latitude", String.valueOf(0.0)));
                        Double longi = Double.parseDouble(pref.getString("longitude", String.valueOf(0.0)));
                        LatLng start = new LatLng(lati, longi);
                        LatLng end = marker.getPosition();  // współrzędne markera

                        Log.d("start", start.toString());
                        Log.d("end", end.toString());

                        //Pobiera adres do Google Directions API
                        String url = getDirectionsUrl(start, end);

                        DownloadTask downloadTask = new DownloadTask();

                        // Pobiera dane z Google Directions API
                        downloadTask.execute(url);

                        return false;
                    }
                });

                //Przegląda wszystkie obiekty z bazy i pobiera współrzędne
                titles.clear();
                int i = 0;

                for (Locations ev : listAllEvents) {
                    LatLongList.add(i, new LatLng(ev.getLatitude(), ev.getLongitude()));
                    titles.add(i, ev.getTitle());
                    i++;
                }

                //Przyciski interfejsu mapy
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;
                }
                googleMap.setMyLocationEnabled(true);
                googleMap.setBuildingsEnabled(true);
                googleMap.setIndoorEnabled(true);

                //Dodanie wszystkich markerów do mapy
                for (i = 0; i < titles.size(); i++) {

                    markerOp = new MarkerOptions().position(LatLongList.get(i)).title(titles.get(i));
                    markerOp.icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_RED));

                    googleMap.addMarker(markerOp);
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(LatLongList.get(i)).zoom(12).build();
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }
            }
        });


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        db.close();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {

        }
    }

    /**
     * Metoda zwracająca gotowy link z parametrami do Google Directions API
     */
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Poczatek trasy
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Cel trasy
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor wlaczony
        String sensor = "sensor=false";

        // Tryb pieszego dla trasowania
        String mode = "mode=walking";

        // Parametry dla web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Format wyjsciowy jako json
        String output = "json";

        // Budowanie url
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    /**
     * Metoda pobierajaca dane json z linku
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Tworzenie polaczenia HTTP z urlem
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();

            // Odczyt danych z urla
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    /**
     * Klasa odpowiadająca za pobieranie danych z serwisu
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                // Dane z web service-u
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    /**
     * Klasa parsujaca dane z Google Places w formacie json
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionJsonParser parser = new DirectionJsonParser();

                // Parsowanie
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        //

        /**
         * Metoda do wykonywania watku po parsowaniu (dodanie punktów, rysowanie linii)
         */
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            String distance = "";
            // Wszystkie trasy
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Pobiera dystans
                        distance = (String) point.get("distance");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                //Dodanie wszystkich punktów do linii
                lineOptions.addAll(points);
                lineOptions.width(5);

                Random rnd = new Random();

                lineOptions.color(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
                lineOptions.clickable(true);


            }

            try {
                // rysowanie linii
                mMapView.getMap().addPolyline(lineOptions);
                Toast.makeText(getContext(), "Distance: " + distance, Toast.LENGTH_SHORT).show();
            } catch (NullPointerException e) {
                Log.e("Error", "Błąd linii");
            }

        }
    }


}













