package com.example.micha.lokalizator.service;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.micha.lokalizator.database.DatabaseHandler;
import com.example.micha.lokalizator.models.Locations;
import com.example.micha.lokalizator.NotificationReceiver;
import com.example.micha.lokalizator.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class ServiceGPS extends IntentService
        implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    DatabaseHandler db = null;
    private int value, range, freq;
    private long freq_milis;
    private boolean isNotification, val;
    SharedPreferences pref;
    SharedPreferences pref_track;
    private static List<Double> listLon = new ArrayList<Double>();
    private static List<Double> listLat = new ArrayList<Double>();
    private static List<String> listTitle = new ArrayList<String>();
    private static List<String> listDate = new ArrayList<String>();
    private static List<String> listTime = new ArrayList<String>();
    SharedPreferences.Editor editor;
    private int count;
    private Handler handler_time;
    private Runnable runnable;


    public ServiceGPS() {
        super("ServiceGPS");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        count = 0;
        try {
            Log.d("COUNT", String.valueOf(count));
            db = new DatabaseHandler(getBaseContext()); //łaczymy sie z bazą
        } catch (Exception e) {
            Log.e("Err", "Błąd bazy");
        }

        pref_track = getApplicationContext().getSharedPreferences("TRACK", MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences("DISTANCE", Activity.MODE_PRIVATE);
        if (pref != null) {
            freq = pref.getInt("shared_frequency", 15);
            freq_milis = TimeUnit.MINUTES.toMillis(freq);

        }
        //Ustawienia dla lokalizacji
        if (isGooglePlayServicesAvailable()) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10);   /* interwały czasowe miedzy aktualizacjami - w milisekundach */
            mLocationRequest.setFastestInterval(5);     /* najszybsze możliwe interwały czasowe miedzy aktualizacjami - w milisekundach */
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);   /* priorytet update'ów - tu wysoki */
            mLocationRequest.setSmallestDisplacement(1.0f);  /* minimalny dystans dla zmiany lokacji - tu 1m */
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            mGoogleApiClient.connect();
        }

        final Handler handler = new Handler();
        final Runnable runnableCode = new Runnable() {

            @Override
            public void run() {
                count++;
                if (count == 20) {
                    count = 0;
                }
                handler.postDelayed(this, 1000);    // co 1 sekunde
            }
        };
        handler.post(runnableCode);


        //Handler wywołujacy w interwałach metodę do sprawdzenia czasu do wydarzeń
        handler_time = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {

                difference_between_two_dates();
                handler_time.postDelayed(this, freq_milis);
            }
        };
        handler_time.post(runnable);


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        db.close();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
//        value = intent.getIntExtra("settings", 50);
//        val = intent.getBooleanExtra("settings_notification", true);
//        freq = intent.getIntExtra("settings_frequency", 10);
//        Log.d("freq now",  String.valueOf(freq));
    }

    /**
     * Metoda sprawdza czy Google Play Services są dostępne
     */
    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        return ConnectionResult.SUCCESS == status;
    }


    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    protected void startLocationUpdates() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } catch (IllegalStateException e) {
        }
    }

    /**
     * Publiczna metoda wykonująca wszystkie działania przy każdej zmianie lokalizacji usera
     */
    @Override
    public void onLocationChanged(Location location) {

        range = downloadSettings(getApplicationContext());
        isNotification = notificationSettings(getApplicationContext());
        freq = pref.getInt("shared_frequency", 15);
        freq_milis = TimeUnit.MINUTES.toMillis(freq);

        Log.d("Freq", String.valueOf(freq));


        listLat.clear();
        listLon.clear();
        listTitle.clear();

        List<Locations> listAllEvents = db.getAllEvents(); // Pobranie wszystkich dostępnych lokacji z bazy danych
        int i = 0;

        //Tworzenie list tytułów i współrzędnych eventów z bazy
        for (Locations ev : listAllEvents) {
            listTitle.add(i, ev.getTitle());
            listLat.add(i, ev.getLatitude());
            listLon.add(i, ev.getLongitude());
            i++;
        }

        for (int n = 0; n < listLat.size(); n++) {

            double LatEnd = listLat.get(n);
            double LongEnd = listLon.get(n);

            Location staticLoc = new Location("Loc");
            staticLoc.setLatitude(LatEnd);
            staticLoc.setLongitude(LongEnd);

            //Wysłanie współrzędnych do trasowania dla mapy
            editor = pref_track.edit();
            editor.putString("latitude", String.valueOf(location.getLatitude()));
            editor.putString("longitude", String.valueOf(location.getLongitude()));
            editor.apply();

            //Obliczenie dystansu usera do lokalizacji
            double distance = location.distanceTo(staticLoc);

            //Jeżeli są wlączone notyfikacje: isNotification = true, sprawdzane jest czy obecny dystans do każdego
            //obiektu jest mniejszy badz równy wprowadzonej przez usera wartości (range). Jeżeli tak, pokazywane jest powiadomienie
            if (isNotification) {
                Log.d("COUNT_LOCAT", String.valueOf(count));
                if (distance <= range && (count >= 19 && count <= 20)) {
                    showNotification(n, listTitle.get(n), distance);
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * Metoda budująca powiadomienie dla skanera dystansu
     */
    public void showNotification(int id, String title, double distance) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("You are close to the place!")
                        .setSmallIcon(R.drawable.ic_place)
                        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),
                                R.drawable.ic_place))
                        .setColor(getApplicationContext().getResources().getColor(R.color.colorPrimaryDark))
                        .setContentText("Distance to event place " + title + " is " + String.format("%.2f", distance) + "m");

        NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(soundUri);
        builder.getNotification().flags |= android.app.Notification.FLAG_AUTO_CANCEL;
        nManager.notify(id, builder.build());
    }

    /**
     * Metoda obliczająca różnicę między obecnym czasem, a datami i czasem w wydarzeniach
     */
    public void difference_between_two_dates() {

        listTime.clear();
        listDate.clear();
        listTitle.clear();

        List<Locations> listAllEvents = db.getAllEvents(); // Pobranie wszystkich dostępnych lokacji z bazy danych
        int i = 0;

        for (Locations ev : listAllEvents) {
            listTitle.add(i, ev.getTitle());
            listDate.add(i, ev.getDate());
            listTime.add(i, ev.getTime());
            i++;
        }

        // Obliczanie różnicy w godzinach pomiędzy obecną datą i czasem, a wszystkimi z bazy danych
        long timestamp = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String dateStart = sdf.format(timestamp);
        Log.i("DATE_START", dateStart);
        for (int n = 0; n < listTitle.size(); n++) {

            String dateStop = listDate.get(n) + " " + listTime.get(n);
            Log.i("DATE_STOP", dateStop);

            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            Date d1 = null;
            Date d2 = null;

            try {
                d1 = format.parse(dateStart);
                d2 = format.parse(dateStop);

                //w milisekundach
                long diff = d2.getTime() - d1.getTime();
                long diff_hours = TimeUnit.MILLISECONDS.toHours(diff); //konwersja do godzin
                long diff_mins = TimeUnit.MILLISECONDS.toMinutes(diff); //konwersja do minut
                Log.i("DIFF", String.valueOf(diff_hours));
                Log.i("DIFF", String.valueOf(diff_mins));


                Intent intent = new Intent(getApplicationContext(), NotificationReceiver.class);
                intent.putExtra("signal_value", diff_hours);
                intent.putExtra("signal_value_mins", diff_mins);
                intent.putExtra("signal_title", listTitle.get(n));
                intent.putExtra("signal_time", listTime.get(n));
                sendBroadcast(intent);

            } catch (ParseException e) {

                e.printStackTrace();
            }
        }
    }

    private int downloadSettings(Context context) {

        value = pref.getInt("settings", 45);

        return value;
    }

    private boolean notificationSettings(Context context) {

        val = pref.getBoolean("settings_notification", true);

        return val;
    }


}