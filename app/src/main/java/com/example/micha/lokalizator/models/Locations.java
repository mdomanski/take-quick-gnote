package com.example.micha.lokalizator.models;

/**
 * Created by Michał on 18.03.2017.
 */


/**
 * Klasa danych do bazy
 */
public class Locations {

    private long rowId;
    private String title;
    private String address;
    private String date;
    private String time;
    private double longitude;
    private double latitude;
    private String info;
    private boolean priority;

    public Locations() {

    }

    public Locations(String title, String address, String date, String time, double longitude, double latitude, String info, boolean priority) {
        this.title = title;
        this.address = address;
        this.date = date;
        this.time = time;
        this.longitude = longitude;
        this.latitude = latitude;
        this.info = info;
        this.priority = priority;
    }

    public Locations(long rowId, String title, String address, String date, String time, double longitude, double latitude, String info, boolean priority) {
        this.rowId = rowId;
        this.title = title;
        this.address = address;
        this.date = date;
        this.time = time;
        this.longitude = longitude;
        this.latitude = latitude;
        this.info = info;
        this.priority = priority;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isPriority() {
        return priority;
    }

    public void setPriority(boolean priority) {
        this.priority = priority;
    }


}
