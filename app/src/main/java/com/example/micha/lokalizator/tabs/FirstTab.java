package com.example.micha.lokalizator.tabs;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.micha.lokalizator.database.DatabaseHandler;
import com.example.micha.lokalizator.pickers.DatePickerFragment;
import com.example.micha.lokalizator.adapters.PlaceAutoCompleteAdapter;
import com.example.micha.lokalizator.R;
import com.example.micha.lokalizator.pickers.TimePickerFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

/**
 * Wprowadzanie danych dla nowego zadania/eventu i dodanie do bazy danych
 */
public class FirstTab extends Fragment implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private Button add, calendar, timeBtn;
    private double lon;
    private double lat;
    private String titleText, addressText, infoText, dateText, timeText;
    private EditText title, date, info, time;
    private CheckBox priority;
    private boolean isChecked;
    DatabaseHandler db;
    private LatLng location;
    private PlaceAutoCompleteAdapter mAdapter;
    AutoCompleteTextView address;
    protected GoogleApiClient mGoogleApiClient;
    protected LatLng end;
    private static final LatLngBounds LNG_BOUNDS = new LatLngBounds(new LatLng(-57.965341647205726, 144.9987719580531),
            new LatLng(72.77492067739843, -9.998857788741589));

    public FirstTab() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_first_tab, container, false);

        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addApi(Places.GEO_DATA_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
        mAdapter = new PlaceAutoCompleteAdapter(getContext(), android.R.layout.simple_list_item_1,
                mGoogleApiClient, LNG_BOUNDS, null);

        title = (EditText) view.findViewById(R.id.nameField);
        title.getBackground().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        address = (AutoCompleteTextView) view.findViewById(R.id.addressField);
        date = (EditText) view.findViewById(R.id.dateField);
        calendar = (Button) view.findViewById(R.id.calendarBtn);
        time = (EditText) view.findViewById(R.id.timeField);
        info = (EditText) view.findViewById(R.id.infoField);
        priority = (CheckBox) view.findViewById(R.id.proBox);
        timeBtn = (Button) view.findViewById(R.id.bchange_time);

        address.setAdapter(mAdapter);
        address.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);
                Log.i("TAG", "Autocomplete item selected: " + item.description);

                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (!places.getStatus().isSuccess()) {
                            // Request zakończony niepowodzeniem
                            Log.e("TAG", "Place query did not complete. Error: " + places.getStatus().toString());
                            places.release();
                            return;
                        }
                        // Pobiera obiekt Place z bufora.
                        final Place place = places.get(0);
                        end = place.getLatLng();
                    }
                });

            }
        });

        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (end != null) {
                    end = null;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        add = (Button) view.findViewById(R.id.addButton);
        add.bringToFront();
        add.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                try {

                    titleText = title.getText().toString();
                    addressText = address.getText().toString();
                    dateText = date.getText().toString();
                    infoText = info.getText().toString();
                    timeText = time.getText().toString();
                    isChecked = priority.isChecked();

                    //Sprawdzenie czy wszystkie pola są uzupełnione. Jeżeli nie, wyświetlany jest dialog o brakujących danych
                    if (titleText.equals("") || addressText.equals("") || dateText.equals("") || infoText.equals("") || timeText.equals("")) {
                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setTitle("Missing data");
                        alertDialog.setMessage("Fill in missing data");
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();

                    } else {

                        try {
                            location = getLocationFromAddress(getContext(), addressText);
                            lon = location.longitude;
                            lat = location.latitude;

                            try {

                                db = new DatabaseHandler(getActivity()); //łaczymy sie z bazą
                                db.getWritableDatabase();
                                db.addEvent(titleText, addressText, lon, lat, infoText, dateText, timeText, isChecked);
                                db.close();

                            } catch (Exception e) {
                                Log.e("Err", "Błąd bazy");
                            }
                            Toast.makeText(getActivity(), "New event added", Toast.LENGTH_SHORT).show();
                        } catch (NullPointerException ex) {

                            //Błąd dodania lokalizacji
                            AlertDialog alertDialogErr = new AlertDialog.Builder(getActivity()).create();
                            alertDialogErr.setTitle("Error");
                            alertDialogErr.setMessage("Can't add location");
                            alertDialogErr.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialogErr.show();
                        }

                        //Czyszczenie pól po dodaniu eventu
                        title.setText("");
                        address.setText("");
                        date.setText("");
                        info.setText("");
                        time.setText("");
                        lon = 0.0;
                        lat = 0.0;
                        priority.setChecked(false);
                    }
                } catch (IndexOutOfBoundsException e) {
                    AlertDialog alertDialogErr = new AlertDialog.Builder(getActivity()).create();
                    alertDialogErr.setTitle("Error");
                    alertDialogErr.setMessage("Location not found");
                    alertDialogErr.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialogErr.show();
                }
            }
        });

        return view;
    }

    /**
     * Publiczna metoda odpowiadająca za akcję do wywołania DatePickera i TimePickera
     */
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        timeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerDialog();
            }
        });

    }

    /**
     * Publiczna metoda dialogu DatePickera
     */
    private void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        Calendar calender = Calendar.getInstance();

        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);

        date.setCallBack(ondate);
        date.show(getFragmentManager(), "Date Picker");
    }


    //Pobranie daty z datepickera i ustawienie jej do EditTextu
    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {


            view.setMinDate(System.currentTimeMillis() - 1000);
            date.setText(String.valueOf(dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + String.valueOf(year)); //dd-mm-yyyy

        }

    };

    /**
     * Publiczna metoda dialogu TimePickera
     */
    public void showTimePickerDialog() {
        DialogFragment newFragment = new TimePickerFragment(mTimeSetListener);
        newFragment.show(getActivity().getFragmentManager(), "timePicker");
    }

    //Pobranie czasu z timepickera i ustawienie jej do EditTextu
    TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(android.widget.TimePicker view,
                                      int hourOfDay, int minute) {

            time.setText(String.format("%02d:%02d", hourOfDay, minute)); //format HH:mm
                }
            };

    /**
     * Metoda przetwarzająca adres na współrzędne geograficzne
     */
    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;

        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            Log.d("Err", "getLocationFromAddress: " + address);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
