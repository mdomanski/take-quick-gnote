package com.example.micha.lokalizator;


import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.micha.lokalizator.service.ServiceGPS;
import com.example.micha.lokalizator.tabs.FirstTab;
import com.example.micha.lokalizator.tabs.FourthTab;
import com.example.micha.lokalizator.tabs.SecondTab;
import com.example.micha.lokalizator.tabs.ThirdTab;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa głównej aktywności
 */
public class MainActivity extends AppCompatActivity  {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_add,
            R.drawable.ic_place,
            R.drawable.ic_assign,
            R.drawable.ic_settings,
    };
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (isMyServiceRunning(ServiceGPS.class)) {
            Log.d("Service", "Service jest już uruchomiony");
        } else {
            startService(view); //start serwisu gps
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            viewPager.setOffscreenPageLimit(0);
        }
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        android.support.v7.app.ActionBar menu = getSupportActionBar();
        if (menu != null) {
            menu.setDisplayShowHomeEnabled(true);
            menu.setLogo(R.drawable.logo);
            menu.setDisplayUseLogoEnabled(true);

        }
    }

    /**
     * Metoda uruchamiająca serwis GPS
     */
    public void startService(View view) {
        startService(new Intent(getBaseContext(), ServiceGPS.class));
    }

    /**
     * Metoda ustawiająca ikony dla kart tabLayoutu
     */
    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[2]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[0]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);

    }

    /**
     * Metoda dodająca fragmenty (karty) do adaptera
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ThirdTab(), "Trzecia");
        adapter.addFragment(new SecondTab(), "Druga");
        adapter.addFragment(new FirstTab(), "Pierwsza");
        adapter.addFragment(new FourthTab(), "Czwarta");
        viewPager.setAdapter(adapter);

    }


    /**
     * Klasa adaptera fragmentów dla tablayoutu
     */
    class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);

        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
            return null;
        }


        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.info:
                AlertDialog.Builder builderInfo = new AlertDialog.Builder(this);
                builderInfo.setMessage("Take quick Gnote\nAn application that creates location notes.\nIcons: material.io/icons")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }

                        });
                AlertDialog alert = builderInfo.create();
                alert.show();
                return true;

            case R.id.author:
                AlertDialog.Builder builderAuthor = new AlertDialog.Builder(this);
                builderAuthor.setMessage("Author: Michał Domański\nContact: mdomanski92@gmail.com")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }

                        });
                AlertDialog alert2 = builderAuthor.create();
                alert2.show();

                return true;
        }
        return false;
    }

    /**
     * Metoda sprawdza czy serwis został uruchomiony
     */
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
