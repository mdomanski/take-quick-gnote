package com.example.micha.lokalizator.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.micha.lokalizator.models.Locations;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHandler extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "locationsDB";
    private static final String TABLE_LOC = "locations";


    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_DATE = "date";
    private static final String KEY_TIME = "time";
    private static final String KEY_LON = "longitude";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_INFO = "info";
    private static final String KEY_PRO = "priority";


    private static final String CREATE_LOCATIONS_TABLE = "CREATE TABLE " + TABLE_LOC + "("
            + KEY_ID + " INTEGER PRIMARY KEY, "
            + KEY_TITLE + " TEXT, "
            + KEY_ADDRESS + " TEXT, "
            + KEY_DATE + " DATE, "
            + KEY_TIME + " DATETIME, "
            + KEY_LON + " DOUBLE, "
            + KEY_LAT + " DOUBLE, "
            + KEY_INFO + " TEXT, "
            + KEY_PRO + " BOOLEAN "
            + ");";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_LOCATIONS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOC);
        onCreate(db);
    }

    /**
     * Publiczna metoda, która dodaje nowe wydarzenie do bazy danych
     */
    public long addEvent(String title, String address, Double lon, Double lat, String info, String date, String time, Boolean priority) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues val = new ContentValues();
        val.put("title", title);
        val.put("address", address);
        val.put("date", date);
        val.put("time", time);
        val.put("longitude", lon);
        val.put("latitude", lat);
        val.put("info", info);
        val.put("priority", priority);

        long rowId = db.insert(TABLE_LOC, null, val);
        db.close();

        return rowId;
    }

    /**
     * Publiczna metoda zwracajaca wszystkie obiekty z bazy danych
     */
    public List<Locations> getAllEvents() {

        List<Locations> eventList = new ArrayList<Locations>();


        SQLiteDatabase db = this.getReadableDatabase();     // nadanie możliwości odczytu bazy
        String selectQuery = "SELECT * FROM " + TABLE_LOC;  //zapytanie zwracajace wszystkie elementy z tabeli

        try {
            Cursor crs = db.rawQuery(selectQuery, null);    // obiekt Cursora

            if (crs.moveToFirst()) {
                do {
                    Locations obj = new Locations();
                    obj.setRowId(crs.getLong(0));
                    obj.setTitle(crs.getString(1));
                    obj.setAddress(crs.getString(2));
                    obj.setDate(crs.getString(3));
                    obj.setTime(crs.getString(4));
                    obj.setLongitude(crs.getDouble(5));
                    obj.setLatitude(crs.getDouble(6));
                    obj.setInfo(crs.getString(7));
                    obj.setPriority(crs.getInt(8) > 0);
                    eventList.add(obj);
                } while (crs.moveToNext());

            }

            crs.close();

        } catch (SQLiteException e) {
            Log.e("sqLite-getdata", "getDataSQL", e.fillInStackTrace());
        } catch (Exception e) {
            Log.e("sqLite-getdata", "getDataException", e.fillInStackTrace());
            Log.e("info", e.getMessage(), e.fillInStackTrace());
        }
        return eventList;     //zwraca liste wydarzeń

    }


//    public void removeFromDB(String id) {
//        String selectQuery = "DELETE FROM " + TABLE_LOC + " WHERE id = '" + id + "'";
//        Log.i("info", "removeFromDB: " + selectQuery);
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL(selectQuery);
//        db.close();
//    }
    /**
     * Publiczna metoda usuwająca z bazy rekord
     */
    public void removeFromDBbyTitle(String title) {
        String selectQuery = "DELETE FROM " + TABLE_LOC + " WHERE title = '" + title + "'";
        Log.i("info", "removeFromDB: " + selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }


    /**
     * Publiczna metoda czyszczaca baze danych
     */
    public void clearDB() {
        String selectQuery = "DELETE FROM " + TABLE_LOC;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(selectQuery);
        db.close();
    }
}
