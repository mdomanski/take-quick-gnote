package com.example.micha.lokalizator.tabs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.micha.lokalizator.adapters.CardAdapter;
import com.example.micha.lokalizator.database.DatabaseHandler;
import com.example.micha.lokalizator.R;
import com.example.micha.lokalizator.models.Locations;

import java.util.ArrayList;
import java.util.List;


public class ThirdTab extends Fragment {


    DatabaseHandler db = null;

    // Listy składowe, do których zapisywane są poszczególne elementy z bazy.

    private static List<String> listTitle = new ArrayList<String>();    //tytuły
    private static List<String> listAddress = new ArrayList<String>();    //adresy
    private static List<String> listInfo = new ArrayList<String>();     //dodatkowe info
    private static List<String> listDate = new ArrayList<String>();     //daty
    private static List<Boolean> listPriority = new ArrayList<Boolean>(); //priorytety
    private static List<String> listTime = new ArrayList<String>();

    public ThirdTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third_tab, container, false);
        try {

            db = new DatabaseHandler(getActivity()); //łaczymy sie z bazą

        } catch (Exception e) {
            Log.e("Err", "Błąd bazy");
        }
        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);

        List<Locations> listAllEvents = db.getAllEvents(); // Pobranie wszystkich dostępnych lokacji z bazy danych
        int i = 0;

        // Czyszczenie list tytularnych, aby nie klonowalo itemów
        listTitle.clear();
        listDate.clear();
        listTime.clear();
        listPriority.clear();
        listAddress.clear();
        listInfo.clear();

        // Przeglądanie obiekt po obiekcie elementów z bazy i dodanie ich składowych do odpowiednich list.
        for (Locations ev : listAllEvents) {

            listTitle.add(i, ev.getTitle());
            listDate.add(i, ev.getDate());
            listTime.add(i, ev.getTime());
            listPriority.add(i, ev.isPriority());
            listAddress.add(i, ev.getAddress());
            listInfo.add(i, ev.getInfo());
            i++;

        }

        CardAdapter adapter = new CardAdapter(listTitle, listDate, listTime, listPriority, listAddress, listInfo);
        adapter.notifyDataSetChanged();
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        return view;
    }

}
