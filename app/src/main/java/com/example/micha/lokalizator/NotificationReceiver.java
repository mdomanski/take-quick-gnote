package com.example.micha.lokalizator;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Klasa receivera, która sprawdza i przypomina o nadchodzącym zadaniu/evencie.
 */
public class NotificationReceiver extends BroadcastReceiver {

    private long value, value_mins;
    private String title, time;
    private static int id = 1;
    SharedPreferences pref;
    private boolean isNotification, val;

    /**
     * Metoda pobierająca dane do powiadomienia
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        value = intent.getExtras().getLong("signal_value");
        value_mins = intent.getExtras().getLong("signal_value_mins");
        title = intent.getExtras().getString("signal_title");
        time = intent.getExtras().getString("signal_time");
        isNotification = notificationSettings(context);

        //Sprawdzenie czy opcja notyfikacji jest aktywna i czy do wydarzenia została godzina
        if (isNotification) {
            if (value == 0 && value_mins > 0) {
                showNotification(context, id, title, value_mins);    //wywołanie powiadomienia
                id++;
            }
        }
    }

    /**
     * Metoda budująca powiadomienie
     */
    public void showNotification(Context context, int id, String title, long value_mins) {
        NotificationCompat.Builder builder;
        if (value_mins == 1) {
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle("Reminder")
                    .setSmallIcon(R.drawable.ic_time)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                            R.drawable.ic_time))
                    .setColor(context.getResources().getColor(R.color.colorPrimaryDark))
                    .setContentText("Remaining " + value_mins + " minute to start " + title);
        } else {
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle("Reminder")
                    .setSmallIcon(R.drawable.ic_time)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                            R.drawable.ic_time))
                    .setColor(context.getResources().getColor(R.color.colorPrimaryDark))
                    .setContentText("Remaining " + value_mins + " minutes to start " + title);
        }

        NotificationManager nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(soundUri);
        builder.getNotification().flags |= android.app.Notification.FLAG_AUTO_CANCEL;
        nManager.notify(id, builder.build());
    }

    /**
     * Metoda pobierająca aktualne ustawienie dla powiadomień czasowych
     */
    private boolean notificationSettings(Context context) {

        try {
            val = pref.getBoolean("settings_notification", true);

        } catch (NullPointerException e) {
            val = true;
        }

        return val;
    }
}
