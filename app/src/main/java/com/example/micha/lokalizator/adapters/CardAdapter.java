package com.example.micha.lokalizator.adapters;

/**
 * Created by Michał on 20.03.2017.
 */

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.micha.lokalizator.database.DatabaseHandler;
import com.example.micha.lokalizator.R;

import java.util.List;

/**
 * Klasa adaptera odpowiadająca za budowę CardView
 */
public class CardAdapter extends RecyclerView.Adapter<CardAdapter.MyViewHolder> {
    private List<String> mDataset;
    private List<String> SubmDataset;
    private List<String> timeDataset;
    private List<String> addrDataset;
    private List<String> infoDataset;
    private List<Boolean> prioDataset;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView mTextView, subTextView, addrView, infoView;
        Button deleteItem;
        ImageView image;
        DatabaseHandler db;

    public MyViewHolder(View v) {
            super(v);

            mCardView = (CardView) v.findViewById(R.id.card_view);
            mTextView = (TextView) v.findViewById(R.id.tv_text);
            subTextView = (TextView) v.findViewById(R.id.tv_blah);
            addrView = (TextView) v.findViewById(R.id.addr);
            infoView = (TextView) v.findViewById(R.id.infoView);
            deleteItem = (Button) v.findViewById(R.id.cardButton);
            image = (ImageView) v.findViewById(R.id.iv_image);
            db = new DatabaseHandler(v.getContext());
        }
    }

    // Konstruktor dla danych
    public CardAdapter(List<String> myDataset, List<String> myDatasetSecond, List<String> myDatasetThird, List<Boolean> prioData, List<String> myAddrData, List<String> myInfoData) {
        mDataset = myDataset;
        SubmDataset = myDatasetSecond;
        timeDataset = myDatasetThird;
        prioDataset = prioData;
        addrDataset = myAddrData;
        infoDataset = myInfoData;
    }

    // Tworzy nowy widok cardview
    @Override
    public CardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    /**
     * Metoda akcji na kartach
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.mTextView.setText(mDataset.get(position));
        holder.subTextView.setText(SubmDataset.get(position) + " " + timeDataset.get(position));


        holder.mCardView.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {

            }
        });

        holder.deleteItem.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {
                holder.db.removeFromDBbyTitle(mDataset.get(position));
                mDataset.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, mDataset.size());
            }
        });

        if(prioDataset.get(position)){
            holder.image.setBackgroundResource(R.drawable.ic_pin);
//            holder.mCardView.setCardBackgroundColor(Color.parseColor("#FFECB3"));
            holder.image.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF9800")));

        }
        holder.addrView.setText(addrDataset.get(position));
        holder.infoView.setText(infoDataset.get(position));

    }


    /**
     * Zwraca liczbę elementów na podstawie długości listy
     */
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    /**
     * Zwraca id itemu na podstawie pozycji
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
