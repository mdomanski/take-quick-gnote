package com.example.micha.lokalizator.tabs;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.example.micha.lokalizator.database.DatabaseHandler;
import com.example.micha.lokalizator.R;

import static android.content.Context.MODE_PRIVATE;

/**
 * Klasa ustawień aplikacji
 */
public class FourthTab extends Fragment {

    int value, freq_value;
    private Button clearBtn, applyBtn;
    private EditText range, frequency;
    private Switch notification;
    DatabaseHandler db;
    SharedPreferences.Editor editor;
    public static final String NAME = "DISTANCE";
    private boolean isChecked;

    public FourthTab() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_fourth_tab, container, false);
        try {
            db = new DatabaseHandler(getActivity()); //łaczymy sie z bazą
        } catch (Exception e) {
            Log.e("Err", "Błąd bazy");
        }
        SharedPreferences pref = getContext().getSharedPreferences(NAME, MODE_PRIVATE);
        editor = pref.edit();

        frequency = (EditText) view.findViewById(R.id.freqField);
        range = (EditText) view.findViewById(R.id.rangeField);
        notification = (Switch) view.findViewById(R.id.notificationSwitch);

        //Przycisk czyszczący baze
        clearBtn = (Button) view.findViewById(R.id.clearAllButton);
        clearBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //Czyszczenie całej bazy
                db.clearDB();
                Toast.makeText(getActivity(), "All events deleted", Toast.LENGTH_SHORT).show();
            }
        });

        //Przycisk zatwierdzający zmianę ustawień
        applyBtn = (Button) view.findViewById(R.id.applyBtn);
        applyBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                isChecked = notification.isChecked();   //checkbox notyfikacji
                editor.putBoolean("settings_notification", isChecked);

                if (!range.getText().toString().equals("")) {
                    value = Integer.parseInt(range.getText().toString());
                    editor.putInt("settings", value);
                    Toast.makeText(getActivity(), "Searching range set at " + value + "m", Toast.LENGTH_SHORT).show();

                }
                if (!frequency.getText().toString().equals("")) {
                    freq_value = Integer.parseInt(frequency.getText().toString());
                    editor.putInt("shared_frequency", freq_value);
                    Toast.makeText(getActivity(), "Frequency set at " + freq_value + " min", Toast.LENGTH_SHORT).show();

                }
                editor.apply();
                editor.commit();
            }
        });
        return view;
    }

}
