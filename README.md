# **Take quick Gnote** #


An application that creates location notes.

### The application uses ###

* Google Maps API
* Google Directions API
* Google Places API
* Fused location provider API
* SQLite database
* Material Design

### Main functions ###

* Interactive map
* Add new and delete tasks
* Prioritizing tasks
* Showing current location on the map
* Notification when we are close to the event
* Notifications of the upcoming event
* Draw route to destination from current location

### Screenshots from current version of app ###

![Screenshot_1491730815.png](https://bitbucket.org/repo/baqA8xe/images/2753926705-Screenshot_1491730815.png) ![Screenshot_1491730819.png](https://bitbucket.org/repo/baqA8xe/images/231767875-Screenshot_1491730819.png) ![Screenshot_1492790672.png](https://bitbucket.org/repo/baqA8xe/images/2366224635-Screenshot_1492790672.png)

![Screenshot_1494843817.png](https://bitbucket.org/repo/baqA8xe/images/3180059819-Screenshot_1494843817.png) ![Screenshot_1491731099.png](https://bitbucket.org/repo/baqA8xe/images/1495960633-Screenshot_1491731099.png) ![Screenshot_1491731103.png](https://bitbucket.org/repo/baqA8xe/images/3590514459-Screenshot_1491731103.png)